#Check run script
$WantFile = "C:\Users\user\ansible_ps_script.txt"
$FileExists = Test-Path $WantFile
If ($FileExists -eq $True) {Write-Host "Script is already executed"} else { 

  $Groups = Get-LocalGroup
  foreach ($Group in $Groups){
    if ($Group -like "Администраторы") { 
      break
    }
    elseif ($Group -like "Administrators") {
      break
    }
  }

  #User to search for
  $username = "user"
  $pass = ConvertTo-SecureString '???????????' -AsPlainText -Force

  #Declare LocalUser Object
  $ObjLocalUser = $null

  Try {
    Write-Host "Searching for $($username) in LocalUser DataBase"
    $ObjLocalUser = Get-LocalUser $username
    Write-Host "User $($username) was found"
    Set-LocalUser -Name "user" -PasswordNeverExpires 1
  }

  Catch  {
    "User $($username) was not found" | Write-Host
  }

  #Create the user if it was not found
  If (!$ObjLocalUser) {
    Write-Host "Creating User $($username)" 
    New-LocalUser $username -Password $pass -FullName "user"
    Set-LocalUser -Name $username -PasswordNeverExpires 1
    Add-LocalGroupMember -Group "$Group" -Member $username
  }

  #Find admin name
  $Admins = Get-LocalUser
  foreach ($adminname in $Admins){
    if ($adminname -like "admin") { 
      break
    }
    elseif ($adminname -like "Administrator") {
      break
    }
  }

  #Upgrade PS & NET
  [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
  $url = "https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Upgrade-PowerShell.ps1"
  $file = "$env:temp\Upgrade-PowerShell.ps1"
  $password = "???????????)"
  (New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)


  # Version can be 3.0, 4.0 or 5.1
  &$file -Version 5.1 -Username $adminname -Password $password -Verbose
  $reg_winlogon_path = "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon"
  Set-ItemProperty -Path $reg_winlogon_path -Name AutoAdminLogon -Value 0
  Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultUserName -ErrorAction SilentlyContinue

  #Configure listeners
  [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
  $url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
  $file = "$env:temp\ConfigureRemotingForAnsible.ps1"
  (New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
  powershell.exe -ExecutionPolicy ByPass -File $file

  # Only remove listeners that are run over HTTPS
  Get-ChildItem -Path WSMan:\localhost\Listener | Where-Object { $_.Keys -contains "Transport=HTTP" } | Remove-Item -Recurse -Force

  #Download build soft from drive
  $User = "jenkins"
  $PWord = ConvertTo-SecureString -String "??????????" -AsPlainText -Force
  #Check directory C:\Temp
  $WantDirect = "C:\Temp"
  $DirectExists = Test-Path $WantDirect
  If ($DirectExists -eq $False) {mkdir "C:\Temp"} 
    $Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $PWord
    New-PSDrive -Name U -PSProvider FileSystem -Root \\drive\tmp\Distribs\Builds_soft -Credential $Credential -Persist
    Copy-Item -Force -Recurse -Verbose -Exclude "System Volume Information" "U:\*" -Destination "C:\Temp"
    Remove-PSDrive -Name U -Force
    Write-Host "Directory C:\Temp created"
    New-Item -Path "C:\Users\user" -Name "ansible_ps_script.txt" -ItemType "file" -Value "Ansible was here"
}