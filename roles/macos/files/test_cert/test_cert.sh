#!/bin/sh

cd ./scripts
echo "--------------Movavi-------------"

rm ./libNagScreen_1.dylib
cp ./libNagScreen.dylib ./libNagScreen_1.dylib
codesign -v -f -s "Developer ID Application: Movavi Software Inc (U8KD48QDBK)" ./libNagScreen_1.dylib

rm ./libNagScreen_2.dylib
cp ./libNagScreen.dylib ./libNagScreen_2.dylib
codesign -v -f -s "3rd Party Mac Developer Application: Movavi Software Inc. (U8KD48QDBK)" ./libNagScreen_2.dylib

rm ./test_file_plain_1.pkg
productsign --sign "Developer ID Installer: Movavi Software Inc (U8KD48QDBK)" ./test_file_plain.pkg ./test_file_plain_1.pkg

rm ./test_file_plain_2.pkg
productsign --sign "3rd Party Mac Developer Installer: Movavi Software Inc. (U8KD48QDBK)" ./test_file_plain.pkg ./test_file_plain_2.pkg

echo "--------------TAUKONSALT-------------"

rm ./libNagScreen_1_tau.dylib
cp ./libNagScreen.dylib ./libNagScreen_1_tau.dylib
codesign -v -f -s "Developer ID Application: TAUKONSALT, OOO (9SR9CB7C2N)" ./libNagScreen_1_tau.dylib

rm ./libNagScreen_2_tau.dylib
cp ./libNagScreen.dylib ./libNagScreen_2_tau.dylib
codesign -v -f -s "3rd Party Mac Developer Application: TAUKONSALT, OOO (9SR9CB7C2N)" ./libNagScreen_2_tau.dylib

rm ./test_file_plain_1_tau.pkg
productsign --sign "Developer ID Installer: TAUKONSALT, OOO (9SR9CB7C2N)" ./test_file_plain.pkg ./test_file_plain_1_tau.pkg

rm ./test_file_plain_2_tau.pkg
productsign --sign "3rd Party Mac Developer Installer: TAUKONSALT, OOO (9SR9CB7C2N)" ./test_file_plain.pkg ./test_file_plain_2_tau.pkg
