# Movavi DevOps Team Ansible Repository
Запуск плейбука для настройки windows build:
ansible-playbook -i inventory/prod main.yml

Запуск плейбука для настройки windows wuild-slave:
ansible-playbook -i inventory/prod winwuild.yml

Шифрование секретов:
ansible-vault encrypt your_file

Просмотр секретов:
ansible-vault view your_file

Расшифровка секретов:
ansible-vault decrypt your_file

Запуск плэйбука macos:
ansible-playbook macos.yml -i inventory/prod --vault-password-file vault_pass.txt

P.S. vault_pass.txt лежит в https://vault-cluster-01.core.movavi.com:8200/ui/vault/secrets/infrastructure/list/ansible/

P.S.S PREAnsible.ps1 - скрипт по преднастройке windows для работы с ansible, отрабатывает через GPO